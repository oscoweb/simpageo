/*controllers*/
angular.module('starter.controllers', [])

  .controller('AccountCtrl', ['Functions','$scope',function(Functions, $scope) {
    db("AccountCtrl loaded");
    $scope.settings = {
      enableFriends: true
    };
  }])

  .controller('RoutesCtrl', ['Functions','$scope','Routes',function(Functions, $scope, Routes) {
    db("RoutesCtrl loaded");

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    $scope.routes = Routes.all();
    $scope.remove = function(route) {
      Routes.remove(route);
    };
  }])


/* start dashboard controller */
  .controller('DashCtrl', ['$ionicPopup','Functions' ,'$q', '$scope', 'CurrentPath', function($ionicPopup, Functions, $q, $scope, CurrentPath) {
    $scope.google_map;
    db("DashCtrl loaded");
    $scope.locations = CurrentPath.all();

    $scope.startPos;
    $scope.markers = [];

    $scope.distance_kilometers = 0;
    $scope.distance_miles = 0;
    $scope.distance_meters = 0;
    $scope.distance_feet = 0;

    /* Styling markers */
    $scope.record_progress = "";
    $scope.record_stop = "geo_hidden";
    $scope.record_clear = "geo_hidden";

    $scope.setCurrentUI = function(){
      db("setCurrentUI");
      var getStartPos = CurrentPath.getCurrent();
      getStartPos.then(function(position){
        var lt = position.coords.latitude;
        var ln = position.coords.longitude;
        $scope.curLat = lt;
        $scope.curLon = ln;         
        var myLatlng = new plugin.google.maps.LatLng(lt, ln);
        var div = document.getElementById("google_map");
        $scope.google_map = plugin.google.maps.Map.getMap(div);
        $scope.setOptions(myLatlng, defaultzoom);
        $scope.setMyLoc(myLatlng);
      });

      $scope.curLat = CurrentPath.curLat();
      $scope.curLon = CurrentPath.curLon(); 
    };

    setTimeout(function() {

      var getStartPos = CurrentPath.getCurrent();
      getStartPos.then(function(position){
        db("after imeout getStartPos deferred set: " + position);
        $scope.startPos = position;
        CurrentPath.set(position);
        CurrentPath.setCurrent(position);

        var lt = position.coords.latitude;
        var ln = position.coords.longitude;
        var myLatlng = new plugin.google.maps.LatLng(lt, ln);
        db("map options: " + lt + ", " + ln + " zoomed: " + defaultzoom);
        var div = document.getElementById("google_map");
        tmap = plugin.google.maps.Map.getMap(div);
        $scope.google_map = tmap;
        // Functions.setMap(tmap);
        $scope.setOptions(myLatlng, defaultzoom);
        CurrentPath.setInit(true);
      });
    }, 1000);

    $scope.saveMap = function() {
      db("DashCtrl saveMap pressed");
      var allLocs = Functions.allMeasured();
      var markers = $scope.markers;
      db("locations: " + allLocs.length);
      db("markers: " + markers.length);            

      var promptCallback = function(results) {
        var butchoice = results.buttonIndex;
        var butres = results.input1;
        db("You selected button number " + butchoice + " and entered " + butres);

        if (butchoice === 1) {
          db("OK clicked");
          //OK is clicked
          //Save Map
        }

      };

      //default prompt:
      navigator.notification.prompt("Set a Title for the map", promptCallback, "Map Title", ["Ok","Cancel"], "title");

    };

    $scope.startNewRoute = function(){
      db("DashCtrl startNewRoute pressed");

      $scope.record_progress = "geo_hidden";
      $scope.record_stop = "";
      $scope.record_clear = "";

      var getStartPos = CurrentPath.getCurrent();
      getStartPos.then(function(position){
        startPos = position;
        CurrentPath.set(startPos);
        var lt = 52.343583699999996;
        var ln = 4.917626199999972;
        if(startPos != null) {
          lt = startPos.coords.latitude;
          ln = startPos.coords.longitude;
        } 
        db("map options: " + lt + ", " + ln + " zoomed: " + defaultzoom);
        var myLatlng = new plugin.google.maps.LatLng(lt, ln);
        
        var startLat = startPos.coords.latitude;
        var startLon = startPos.coords.longitude;
        db("startlat " + startLat);
        db("startlon " + startLon);

        // var div = document.getElementById("google_map");
        // $scope.google_map = plugin.google.maps.Map.getMap(div);
        $scope.setOptions(myLatlng, defaultzoom);
        $scope.setMarker(myLatlng);
        db("startNewRoute done");
        Functions.startMeasure($scope.google_map);
      });
    };
    $scope.stopNewRoute = function(){
      db("DashCtrl stopNewRoute pressed");
      $scope.record_progress = "";
      $scope.record_stop = "geo_hidden";
      $scope.record_clear = "";

      var allLocs = Functions.allMeasured();
      // var allLatLings = [];

      // angular.forEach(allLocs,function(element, index){
      //   var newlt = new plugin.google.maps.LatLng(element.coords.latitude,element.coords.longitude);
      //   allLatLings.push(newlt);
      // });
      // db(allLatLings.length);
      // $scope.google_map.addPolyline({
      //   points: allLatLings,
      //   'color' : '#AA00FF',
      //   'width': 10,
      //   'geodesic': true
      // }, function(polyline) {
      //   db("line drawn");
      // });

      Functions.stopMeasure();

      CurrentPath.clear();
      CurrentPath.set(allLocs);
      db("locations saved: " + allLocs.length);
      // var cp = CurrentPath.getLast();
      // var lt = cp.coords.latitude;
      // var ln = cp.coords.longitude;
      // db("last position: " + lt + ", " + ln);

      db("calculating distance");

      /*calculate distance*/
      var distance = 0;
      var loc_old = allLocs[0];
      var loc_new = allLocs[0];
      angular.forEach(allLocs, function(element, index){
        pointA = loc_old;
        pointB = element;

        distance = distance +
        Functions.distance(pointA.coords.latitude, pointA.coords.longitude,
                        pointB.coords.latitude, pointB.coords.longitude);

      });
      db("distance in kilometers: " + distance);
      db("distance in meters: " + distance*1000);
      db("distance in miles: " + distance*0.621371192);
      db("distance in feet: " + distance/0.0003048);

      $scope.distance_kilometers = distance;
      $scope.distance_meters = (distance*1000);
      $scope.distance_miles = (distance*0.621371192);
      $scope.distance_feet = (distance/0.0003048);
    };
    $scope.myMarkers = [];
    $scope.hideMyMarkers = function() {
      var markers = $scope.myMarkers;
      angular.forEach(markers, function(element, index){
        element.remove();
      });
      $scope.myMarkers = [];
    };

    $scope.hideMarkers = function() {
      db("DashCtrl stopNewRoute pressed");
      $scope.record_progress = "";
      $scope.record_stop = "geo_hidden";
      $scope.record_clear = "geo_hidden";
      var markers = $scope.markers;
      db("hiding markers: " + markers.length);
      angular.forEach(markers, function(element, index){
        element.remove();
      });
      $scope.markers = [];
      CurrentPath.clear();
      Functions.clearAllMeasured();
      polyline.remove();
    };

    // Triggered on a button click, or some other target
    $scope.showPopup = function() {
      $scope.data = {}

      // An elaborate, custom popup
      var myPopup = $ionicPopup.show({
        template: '<input type="password" ng-model="data.wifi">',
        title: 'Enter Wi-Fi Password',
        subTitle: 'Please use normal things',
        scope: $scope,
        buttons: [
          { text: 'Cancel' },
          {
            text: '<b>Save</b>',
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.data.wifi) {
                //don't allow the user to close unless he enters wifi password
                e.preventDefault();
              } else {
                return $scope.data.wifi;
              }
            }
          }
        ]
      });
      myPopup.then(function(res) {
        console.log('Tapped!', res);
      });
    };
    $scope.setMyLoc = function(myLatlng){
      var icon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAABECAYAAAAV4bwIAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3woQEjk264kkWQAABdBJREFUaN7tmltoHGUUx38z+e+ETenWWNPEFExFpWq1iPVKlAoFHwR9E33wgoKXF0VBvILiS708aYVSEVGrL0Xqgy1FAopWULzSWrC0UquWWjTVGLEbd/dk1od8kXWa7M58M1tS8MAw8H3n/M9/znc532XgfzkxEhQNKOl0YAXQTPj5wcyOLIivlrS0VCqtlbRNUjPFs83pLz3hkZZ0GvA+cB5Q8oBoAHuBdWZ2NIth6EF2UNIYMA6s9iSMs1sNjEsakzTYlUhLugV4s4u97VYze6uQSFcqlVDS1i4TBnhT0tZKpRLmjrSkL4E1J3B8f2Vml3hHWtKHnoRfBu5x76yyxvnNHmlJm4FbMzqcNLNT5sD6A1iStbuY2W2pI10qlUY9CAPcmLG87cB0PDpHWtKpwFGfedzMgjYt1/Qg3gQGzOy3TpF+ohspPkcCfLBtpCWVgWoOJ9eY2Udz4K4FPsyB22dmU/NFekPOyLyTsTytbGgX6R+BMwpo1ueA74GzgIcLwPvJzEaOIx1F0WlxHI8v1HV0GIYD9Xr96H+6RxzHT+XEfQ/YOE/dRlfvLbP8gkTX+Bk43afpgEvN7FeH0w/cC6wHHgc2mdmEq1sGfOHZBY+Y2XDQQrgXmAR6i5ybC56za8CS1u6xyGd9Dez0bG0fuxBYFBYwPmon2I6Qk1BOetKNxLZ/IUoTaPxLuqen59jJQLqnp+fYv6RrtVoM7FngpPfUarU42adfXeCkXz1uIJrZpoXMeJbfXLPH3oxYyyWtyJgNVwDLM/rZ227KG8sIdj5wUNLHkiodyFYkfQwcdHZZZGxe0qtWrXrQs/WuAkY76Iw6vczSyus40rt3724CW3xXj11KZlscr7Ygz3uCj3Tq/564z3f8cjP7GtjVhUjHHpi7HJ9UzXW9h5Mzc9bP9ZHXZ+ljh4H9GZ0sylmflP2ORzrSZtYELsropCdnfVIucjzSj2YzqwGbMzg5N2d9q2x2/r2moLvcsrWopWUaaTi/cyepFPm+LulmYGsKZyskLZmn7/6VIQvebGb1+SpT76IzHC+0i2Yaf3vNrO3HZSG9BPjF54ghg1SBYTObLCStOqCHurz6fKQT4UyRbon4u3NN+AXINjO7IY1iZtJ9fX2ler1eL5pxFEVRtVpNNUtlXnU54KKv59akJeyToWYWBHF8JAzDU4ArCiD8gpm9nsUg172KpK+Ai3NAfG1mmVstL+mSW9AMeJiPA8vNLHO2zXUsZmaNIAiu9bENguBaH8K5SQM0Go1dQRCMZiQ82mg0dvn67Cli6MdxfCgMw2XApSnUN5rZyywUkbSjw69AO4rwk+d3oGXAUmZ+sqqY2RZX/g1w4Rwme8xstdO5CfgT+AH4bfauppvRXC9pUpJJiluieKfLbH2SphIRnoqiqM/Z39lSHjucSUnri06x/ZJ2Jki2PgckvSdpYLYVJNWcfs21CpIGnN6BeXBiSTujKOrPG9l1CeC6pA8kPSBpVRu7EUkHJY200bnA4XzgcFv9rPMiXC6Xyy0gJunFhNNI0pCkuyV9IulvSc0oioYSLTXkMP52enc7uyiB96Lz05TULJfLZZ8ojzmAqqShRN2TkibmaeaqpMWSAveuzqM3IenJBO5Qi/6YD+nDznh7onxlij8df5d0tXt30l2ZwN/uyg/7ZMTZLfzK3t7e1k3wT8C+Drb9zFxwdhpU+xweAM7PyoT/TKTvce+zp6enD0la7NYcU25nPQy84nHE0HB2w8D5sz+fSFo8PT19CDg74T9bcnGD7/6Wos+B14A3Zp05vT63Wx90O/J+d0RhwITz9Qszl/LVFrsycDtwB3BZ60mpmT3inREl3Qc8PUdTHwA+BT4DvgOOOWIhMAX8DpwKlN1h4qA7EzkHuBy4kpmfWFplAnjKzF4qJI1Legx4FOhLc9CTZZXrjg+eNbNnurEbD4GBIAhGms3mjcB1ZL8/AfgW2BEEwdvNZvNHYNzMUh0t/wMKLxqjX7o5LgAAAABJRU5ErkJggg==";
      $scope.google_map.addMarker({
        'position': myLatlng,
        'title': 'MyLoc',
        'icon': {
          'url': icon
         }
      }, function(marker) {
        $scope.myMarkers.push(marker);
        db("Marker MyLoc is done");
      });
     };


    $scope.setOptions = function(myLatlng, defaultzoom){
      $scope.google_map.setOptions({
        'backgroundColor': '#f0f0f0',
        'controls': {
          'compass': true,
          'myLocationButton': true,
          'zoom': true,
          'mapTypeControl': true,
          'zoomControlOptions': true
        },
        'gestures': {
          'scroll': true,
          'tilt': true,
          'rotate': true,
          'zoom': true
        },
        'camera': {
          'latLng': myLatlng,
          'zoom': defaultzoom
        }
      });
      db("options done");
    };

    $scope.setMarker = function(myLatlng){
      $scope.google_map.addMarker({
        'position': myLatlng,
        'title': 'Start'
      }, function(marker) {
        $scope.markers.push(marker);
        db("Marker is done");
      });
     };

    db("setCurrentMap done");

  }]).filter('numberEx', ['numberFilter', '$locale',
  function(number, $locale) {

    var formats = $locale.NUMBER_FORMATS;
    return function(input, fractionSize) {
      //Get formatted value
      var formattedValue = number(input, fractionSize);

      //get the decimalSepPosition
      var decimalIdx = formattedValue.indexOf(formats.DECIMAL_SEP);

      //If no decimal just return
      if (decimalIdx == -1) return formattedValue;


      var whole = formattedValue.substring(0, decimalIdx);
      var decimal = (Number(formattedValue.substring(decimalIdx)) || "").toString();

      return whole +  decimal.substring(1);
    };
  }
]);; /* end controller */



      // MAP API: https://github.com/wf9a5m75/phonegap-googlemaps-plugin/wiki/Map

    /*

    map.setOptions({
      'backgroundColor': 'white',
      'mapType': plugin.google.maps.MapTypeId.HYBRID,
      'controls': {
        'compass': true,
        'myLocationButton': true,
        'indoorPicker': true,
        'zoom': true // Only for Android
      },
      'gestures': {
        'scroll': true,
        'tilt': true,
        'rotate': true,
        'zoom': true
      },
      'camera': {
        'latLng': GORYOKAKU_JAPAN,
        'tilt': 30,
        'zoom': 15,
        'bearing': 50
      }
    });

    */


