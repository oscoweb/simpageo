angular.module('starter.services', [])

.factory('Routes', function() {
  // Might use a resource here that returns a JSON array
  // Some fake testing data
  var routes = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'https://pbs.twimg.com/profile_images/578237281384841216/R3ae1n61.png'
  }];

  return {
    all: function() {
      return routes;
    },
    remove: function(route) {
      routes.splice(routes.indexOf(route), 1);
    },
    get: function(routeId) {
      for (var i = 0; i < routes.length; i++) {
        if (routes[i].id === parseInt(routeId)) {
          return routes[i];
        }
      }
      return null;
    }
  };
})

.factory('CurrentPath', ['$q', function($q) {

  var currentPosition; /*current GPS position*/
  var locations = []; /*list of all GPS coordinates visited*/
  var init = false;

  return {
    all: function() {
      return locations;
    },
    init: function() {
      return init;
    },
    setInit: function(seti) {
      init = seti;
    },
    remove: function(location) {
      locations.splice(locations.indexOf(location), 1);
    },
    clear: function(){
      locations = [];
    },
    set: function(location) {
      locations.push(location);
    },
    setCurrent: function(location) {
      currentPosition = location;
    },
    getLast: function() {
      var my_array = locations;
      var last_element = my_array[my_array.length - 1];
      return last_element;
    },
    getCurrent: function() {
      return $q(function(resolve) {
        navigator.geolocation.getCurrentPosition(function(position) {
          db("startPos, currentPos set");
          currentPosition = position;
          resolve(position);
        });        
      })
    },
    get: function(locationId) {
      for (var i = 0; i < locations.length; i++) {
        if (locations[i].id === parseInt(locationId)) {
          return locations[i];
        }
      }
      return null;
    }
  };
}])

.factory('Functions', [function(){
  var geowatcher;
  var currentPosition; /*current GPS position*/
  var locations = []; /*list of all GPS coordinates visited*/
  var map;

  return {
    setMap: function(smap) {
      map = smap;
    },
    getMap: function() {
      return map;
    },
    distance: function(lat1, lon1, lat2, lon2) {
      var R = 6371; // km
      var dLat = (lat2-lat1) * Math.PI / 180;
      var dLon = (lon2-lon1) * Math.PI / 180;
      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
              Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
              Math.sin(dLon/2) * Math.sin(dLon/2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = R * c;
      return d;
    },
    stopMeasure: function(){
      navigator.geolocation.clearWatch(geowatcher);
      db("navigation stopped");
    },
    startMeasure: function(map){
      var div = document.getElementById("google_map");
      var tmap = plugin.google.maps.Map.getMap(div);
      allLatLingsCounter = 0;
      geowatcher = navigator.geolocation.watchPosition(function(position) {
        var currentLat = position.coords.latitude;
        var currentLon = position.coords.longitude;
        db("currentLat " + currentLat);
        db("currentLon " + currentLon);
        locations.push(position);
        if (locations.length > 1) {
          var newlt = new plugin.google.maps.LatLng(locations[locations.length-1].coords.latitude,locations[locations.length-1].coords.longitude);
          var oldlt = new plugin.google.maps.LatLng(locations[locations.length-2].coords.latitude,locations[locations.length-2].coords.longitude);
          tmap.addPolyline({
            points: [newlt,oldlt],
            'color' : '#AA00FF',
            'width': 5,
            'geodesic': true
          });
        }
      });
    },
    allMeasured: function() {
      return locations;
    },
    clearAllMeasured: function() {
      locations = [];
      // polyline.remove();
      var div = document.getElementById("google_map");
      var tmap = plugin.google.maps.Map.getMap(div);
      tmap.clear();
    }
  };
}])

.factory('SavedPath', function() {
  var locations = []; /*list of all GPS coordinates visited*/
  return {
    all: function() {
      return locations;
    },
    get: function(locationId) {
      for (var i = 0; i < locations.length; i++) {
        if (locations[i].id === parseInt(locationId)) {
          return locations[i];
        }
      }
      return null;
    }
  };
});
