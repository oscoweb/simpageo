geo.setCurrentMap = function(){
	db("setting current map! got maps back");

	//db("lat: " + startPos.coords.latitude);
	var lt = 52.343583699999996;
	var ln = 4.917626199999972;
	if(startPos != null) {
		lt = startPos.coords.latitude;
		ln = startPos.coords.longitude;
	} 
	db("map options: " + lt + ", " + ln + " zoomed: " + defaultzoom);
	var myLatlng = new plugin.google.maps.LatLng(lt, ln);

	// MAP API: https://github.com/wf9a5m75/phonegap-googlemaps-plugin/wiki/Map

/*

map.setOptions({
  'backgroundColor': 'white',
  'mapType': plugin.google.maps.MapTypeId.HYBRID,
  'controls': {
    'compass': true,
    'myLocationButton': true,
    'indoorPicker': true,
    'zoom': true // Only for Android
  },
  'gestures': {
    'scroll': true,
    'tilt': true,
    'rotate': true,
    'zoom': true
  },
  'camera': {
    'latLng': GORYOKAKU_JAPAN,
    'tilt': 30,
    'zoom': 15,
    'bearing': 50
  }
});

*/

	map.setOptions({
	  'backgroundColor': '#f0f0f0',
	  'controls': {
	    'compass': true
	  },
	  'gestures': {
	    'scroll': true,
	    'tilt': true,
	    'rotate': true
	  },
	  'camera': {
	    'latLng': myLatlng,
	    'zoom': defaultzoom,
	  }
	});
	map.addMarker({
		'position': myLatlng,
		'title': 'Start'
	}, function() {
		marker.showInfoWindow();
		db("Marker is done");
	});
	db("setCurrentMap done");
};
geo.hasgeo = function(){
	if (navigator.geolocation) {
		db('Geolocation available');
  		return true;
	}
	else {
	  	db('Geolocation is not supported for this Browser/OS version yet.');
	  	return false;
	}
};
geo.calculateDistance = function(lat1, lon1, lat2, lon2) {
  var R = 6371; // km
  var dLat = (lat2-lat1) * Math.PI / 180;
  var dLon = (lon2-lon1) * Math.PI / 180;
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
          Math.sin(dLon/2) * Math.sin(dLon/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c;
  return d;
};
geo.measureDistance = function(){
	db("distance: " + distance);
};
geo.stopMeasuring = function(){
	navigator.geolocation.clearWatch(geowatcher);
	db("navigation stopped");
};
geo.pauze = function(){
	db("navigation pauzed");
};
geo.resume = function(){
	db("navigation resumed");
};
geo.init = function(){
	if (navigator.geolocation) {
		db("init has geo available");

		navigator.geolocation.getCurrentPosition(function(position) {
	    	db("starting to find current location");
			startPos = position;
			var startLat = startPos.coords.latitude;
			var startLon = startPos.coords.longitude;
			db("startlat " + startLat);
			db("startlon " + startLon);
			var div = document.getElementById("google_map");
			map = plugin.google.maps.Map.getMap(div);
			map.addEventListener(plugin.google.maps.event.MAP_READY, geo.setCurrentMap);
			db("maps eventlistener google maps set");
	    }, function(error) {
			db("Error occurred. Error code: " + error.code);
			// error.code can be:
			//   0: unknown error
			//   1: permission denied
			//   2: position unavailable (error response from locaton provider)
			//   3: timed out
	    });
	}
};
geo.currentPosition = function(){
	db("currentPosition");
    geowatcher = navigator.geolocation.watchPosition(function(position) {
      var currentLat = position.coords.latitude;
      var currentLon = position.coords.longitude;
      db("currentLat " + currentLat);
      db("currentLon " + currentLon);
      // var cpos = {"lat":currentLat,"lon":currentLon};
      // path.push(cpos);
      distance =
        geo.calculateDistance(startPos.coords.latitude, startPos.coords.longitude,
                          position.coords.latitude, position.coords.longitude);
      db("distance " + distance);
      // db("positions covered: " + path.length);
    });
};